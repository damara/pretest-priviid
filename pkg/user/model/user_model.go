package model

import (
	"gitlab.com/damara/pretest-priviid/lib/models"
	"gitlab.com/damara/pretest-priviid/pkg/user/structs"
)

type UserModel interface {
	Login(structs.ReqLogin) chan models.Result
}
