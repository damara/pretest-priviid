package handler

import (
	"github.com/labstack/echo"
	"gitlab.com/damara/pretest-priviid/pkg/user/model"
)

type HTTPHandler struct {
	model model.UserModel
}

func NewHTTPHandler(model model.UserModel) *HTTPHandler {
	return &HTTPHandler{model: model}
}

func (h *HTTPHandler) Mount(g *echo.Group) {
	g.POST("/login", h.ToLogin)
}
