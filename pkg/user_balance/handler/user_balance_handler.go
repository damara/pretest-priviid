package handler

import (
	"github.com/labstack/echo"
	"gitlab.com/damara/pretest-priviid/pkg/user_balance/model"
)

type HTTPHandler struct {
	model model.UserBalanceModel
}

func NewHTTPHandler(model model.UserBalanceModel) *HTTPHandler {
	return &HTTPHandler{model: model}
}

func (h *HTTPHandler) Mount(g *echo.Group, auth echo.MiddlewareFunc) {
	g.POST("/balance/top-up", h.Topup, auth)
	g.GET("/balance/check", h.Check, auth)
	g.POST("/balance/transfer", h.Transfer, auth)
	g.GET("/logout", h.ToLogout, auth)
}
