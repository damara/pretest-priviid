package handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	jwtConfig "gitlab.com/damara/pretest-priviid/lib/jwt"

	"github.com/labstack/echo"
	"gitlab.com/damara/pretest-priviid/lib/models"
	"gitlab.com/damara/pretest-priviid/pkg/user_balance/structs"
)

func (h *HTTPHandler) Topup(c echo.Context) error {

	if jwtConfig.CheckLogin(c) == false {
		return models.ToJSON(c).BadRequest("Bad Request")
	}

	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}
	t := structs.TopupBalance{}
	err = json.Unmarshal(body, &t)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}

	dataUser := jwtConfig.GetDataUser(c)

	model := structs.TopupBalance{
		ID:      dataUser.ID,
		Nominal: t.Nominal,
	}

	result := <-h.model.TopupBalance(model)

	if result.Error != nil {
		resp := &models.Response{Code: 400, MessageCode: 0, Message: result.Error.Error()}
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, result.Data)
}

func (h *HTTPHandler) Check(c echo.Context) error {

	if jwtConfig.CheckLogin(c) == false {
		return models.ToJSON(c).BadRequest("Bad Request")
	}

	dataUser := jwtConfig.GetDataUser(c)

	model := structs.User{
		Username: dataUser.Username,
	}

	result := <-h.model.CheckBalance(model)

	if result.Error != nil {
		resp := &models.Response{Code: 400, MessageCode: 0, Message: result.Error.Error()}
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, result.Data)
}

func (h *HTTPHandler) Transfer(c echo.Context) error {

	if jwtConfig.CheckLogin(c) == false {
		return models.ToJSON(c).BadRequest("Bad Request")
	}

	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}
	t := structs.TransfersBalance{}
	err = json.Unmarshal(body, &t)
	if err != nil {
		return models.ToJSON(c).BadRequest("Bad Request")
	}

	dataUser := jwtConfig.GetDataUser(c)

	model := structs.TransfersBalance{
		ID:         dataUser.ID,
		Username:   dataUser.Username,
		Nominal:    t.Nominal,
		TargetUser: t.TargetUser,
	}

	result := <-h.model.TransferBalance(model)

	if result.Error != nil {
		resp := &models.Response{Code: 400, MessageCode: 0, Message: result.Error.Error()}
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, result.Data)
}

func (h *HTTPHandler) ToLogout(c echo.Context) error {

	dataUser := jwtConfig.GetDataUser(c)

	model := structs.User{
		ID: dataUser.ID,
	}

	result := <-h.model.Logout(model)

	if result.Error != nil {
		resp := &models.Response{Code: 400, MessageCode: 0, Message: result.Error.Error()}
		return c.JSON(http.StatusBadRequest, resp)
	}

	return c.JSON(http.StatusOK, result.Data)
}
