package model

import (
	"errors"

	"gitlab.com/damara/pretest-priviid/db"
	"gitlab.com/damara/pretest-priviid/lib/models"
	"gitlab.com/damara/pretest-priviid/pkg/user_balance/structs"
)

type userBalanceModel struct {
	dbConn *db.DbConnection
}

func NewUserBalanceModel(dbConn *db.DbConnection) UserBalanceModel {
	return &userBalanceModel{dbConn: dbConn}
}

var ErrRecordNotFound = errors.New("record not found")

func (r *userBalanceModel) TopupBalance(param structs.TopupBalance) chan models.Result {
	output := make(chan models.Result)

	go func() {
		defer close(output)

		p := param
		q := r.dbConn.DbData

		e := q.Exec(`WITH selectData AS (select * from user_balance where "userId" = ? ), insertUserBalance AS ( UPDATE user_balance set "balanceAchieve" = (select "balanceAchieve" from selectData) + ? WHERE "userId" = ? RETURNING *) INSERT INTO user_balance_history (userbalanceid, balancebefore, balanceafter, activity, "type", ip, "location", useragent, author, created_at) select id , (select "balanceAchieve" from selectData), (select "balanceAchieve" from insertUserBalance), 'top-up', 'debit', '127.0.0.1', 'atm', 'flip', ?, now() from insertUserBalance`, p.ID, p.Nominal, p.ID, p.ID).Error
		if e != nil {
			output <- models.Result{Error: e}
			return
		} else {
			resp := &models.Response{Code: 200, MessageCode: 0, Message: "Success"}
			output <- models.Result{Data: resp}
		}

	}()

	return output
}

func (r *userBalanceModel) CheckBalance(param structs.User) chan models.Result {
	output := make(chan models.Result)
	var checkBalance structs.CheckBalance

	go func() {
		defer close(output)

		p := param
		q := r.dbConn.DbData

		e := q.Raw(`SELECT ub."balanceAchieve" as balance, ubh.balancebefore balance_before 
			FROM users u 
			join user_balance ub on u.id = ub."userId"
			join user_balance_history ubh on ub.id = ubh.userbalanceid  
			where u.username = ? `, p.Username).Scan(&checkBalance).Error
		if e != nil {
			output <- models.Result{Error: e}
			return
		}

		resp := &models.Response{Code: 200, MessageCode: 0, Message: "Success", Data: checkBalance}
		output <- models.Result{Data: resp}

	}()

	return output
}

func (r *userBalanceModel) TransferBalance(param structs.TransfersBalance) chan models.Result {
	output := make(chan models.Result)

	go func() {
		defer close(output)

		p := param
		q := r.dbConn.DbData

		e := q.Exec(`WITH selectData AS (select * from users where username = ? ), selectDataFrom AS (select * from users where username = ? ), 
		selectBalance AS (select * from user_balance where id = ? ), 
		selectBalanceFrom AS (select * from user_balance where id = (select id from selectDataFrom) ),
		insertUserBalance AS ( UPDATE user_balance set "balanceAchieve" = (select "balanceAchieve" from selectBalance) + ? WHERE "userId" = (select id from selectData) RETURNING *), 
		insertAudit AS (INSERT INTO user_balance_history (userbalanceid, balancebefore, balanceafter, activity, "type", ip, "location", useragent, author, created_at) select id , (select "balanceAchieve" from selectBalance), (select "balanceAchieve" from insertUserBalance), concat('tranfer from ', (select username from selectDataFrom)), 'debit', '127.0.0.1', 'atm', 'flip', ?, now() from insertUserBalance ), 
		insertUserBalanceFrom AS ( UPDATE user_balance set "balanceAchieve" = (select "balanceAchieve" from selectBalanceFrom) - ? WHERE "userId" = (select id from selectDataFrom) RETURNING *) 
		INSERT INTO user_balance_history (userbalanceid, balancebefore, balanceafter, activity, "type", ip, "location", useragent, author, created_at) select id , (select "balanceAchieve" from selectBalanceFrom), "balanceAchieve" , concat('tranfer to ', (select username from selectData)), 'debit', '127.0.0.1', 'atm', 'flip', ?, now() from insertUserBalanceFrom`, p.Username, p.TargetUser, p.ID, p.Nominal, p.Username, p.Nominal, p.Username).Error

		if e != nil {
			output <- models.Result{Error: e}
			return
		} else {
			resp := &models.Response{Code: 200, MessageCode: 0, Message: "Success"}
			output <- models.Result{Data: resp}
		}

	}()

	return output
}

func (r *userBalanceModel) Logout(param structs.User) chan models.Result {
	output := make(chan models.Result)
	var users structs.User

	go func() {
		defer close(output)

		p := param
		q := r.dbConn.DbData

		query := "UPDATE users SET islogin = false where id = ? RETURNING *"
		e := q.Raw(query, p.ID).Scan(&users).Error
		if e != nil {
			output <- models.Result{Error: e}
			return
		}

		if users.Islogin == false {
			resp := &models.Response{Code: 200, MessageCode: 10, Message: "Success Logout"}
			output <- models.Result{Data: resp}

		}

	}()

	return output
}
