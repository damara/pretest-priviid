package model

import (
	"gitlab.com/damara/pretest-priviid/lib/models"
	"gitlab.com/damara/pretest-priviid/pkg/user_balance/structs"
)

type UserBalanceModel interface {
	TopupBalance(structs.TopupBalance) chan models.Result
	CheckBalance(structs.User) chan models.Result
	TransferBalance(structs.TransfersBalance) chan models.Result
	Logout(structs.User) chan models.Result
}
