package structs

type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Islogin  bool   `json:"islogin"`
}

type CheckBalance struct {
	Balance       int `json:"balance"`
	BalanceBefore int `json:"balance_before"`
}

type TopupBalance struct {
	Nominal int `json:"nominal"`
	ID      int `json:"id"`
}
type TransfersBalance struct {
	Nominal    int    `json:"nominal"`
	Username   string `json:"username"`
	ID         int    `json:"id"`
	TargetUser string `json:"target_user"`
}
