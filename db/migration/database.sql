-- Drop table

-- DROP TABLE public.users;

CREATE TABLE users (
	id serial NOT NULL,
	username text NOT NULL,
	email text NOT NULL,
	"password" text NOT NULL,
	islogin bool NOT NULL DEFAULT false,
	CONSTRAINT afc_sso_users_pkey PRIMARY KEY (id),
	CONSTRAINT afc_sso_users_user_code_key UNIQUE (email),
	CONSTRAINT users_un UNIQUE (username)
);

-- Drop table

-- DROP TABLE public.user_balance;

CREATE TABLE user_balance (
	id serial NOT NULL,
	"userId" int4 NOT NULL,
	balance varchar(200) NOT NULL,
	"balanceAchieve" int4 NOT NULL,
	CONSTRAINT user_balance_pk PRIMARY KEY (id),
	CONSTRAINT user_balance_fk FOREIGN KEY ("userId") REFERENCES users(id)
);
-- Drop table

-- DROP TABLE public.user_balance_history;


CREATE TYPE enumerasi AS ENUM ('debit', 'kredit');
CREATE TABLE user_balance_history (
	id serial NOT NULL,
	userbalanceid int4 NOT NULL,
	balancebefore int4 NOT NULL,
	balanceafter int4 NOT NULL,
	activity varchar(200) NOT NULL,
	"type" enumerasi NOT NULL,
	ip varchar(200) NOT NULL,
	"location" varchar(200) NOT NULL,
	useragent varchar(200) NOT NULL,
	author varchar(200) NOT NULL,
	created_at timestamp NOT NULL,
	CONSTRAINT user_balance_history_pk PRIMARY KEY (id),
	CONSTRAINT user_balance_history_fk FOREIGN KEY (userbalanceid) REFERENCES user_balance(id)
);

-- Drop table

-- DROP TABLE public.bank_balance;

CREATE TABLE bank_balance (
	id serial NOT NULL,
	balance int4 NOT NULL,
	balance_achieve int4 NOT NULL,
	code varchar(200) NOT NULL,
	"enable" bool NOT NULL,
	CONSTRAINT bank_balance_pk PRIMARY KEY (id)
);
-- Drop table

-- DROP TABLE public.bank_balance_history;

CREATE TABLE bank_balance_history (
	id serial NOT NULL,
	bankbalanceid int4 NOT NULL,
	balancebefore int4 NOT NULL,
	balanceafter int4 NOT NULL,
	activity varchar(200) NOT NULL,
	"type" enumerasi NOT NULL,
	ip varchar(200) NOT NULL,
	"location" varchar(200) NOT NULL,
	useragent varchar(200) NOT NULL,
	author varchar(200) NOT NULL,
	CONSTRAINT bank_balance_history_pk PRIMARY KEY (id),
	CONSTRAINT bank_balance_history_fk FOREIGN KEY (bankbalanceid) REFERENCES bank_balance(id)
);